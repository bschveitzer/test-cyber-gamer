import { Injectable } from '@nestjs/common';
import { ClientProxyFactory, Transport, ClientProxy } from '@nestjs/microservices';
import { IReqLogin } from './interfaces/request.interface';
import { Request } from 'express';

@Injectable()
export class AppService {
  private msAuthClient: ClientProxy;
  private msUserClient: ClientProxy;

  constructor() {
    this.msAuthClient = ClientProxyFactory.create({
      transport: Transport.TCP,
      options: {
        host: '127.0.0.1',
        port: 8877
      }
    }),
    this.msUserClient = ClientProxyFactory.create({
      transport: Transport.TCP,
      options: {
        host: '127.0.0.1',
        port: 8878
      }
    })
  }

  doAuth(data: IReqLogin) {
    return this.msAuthClient.send<string, IReqLogin>('doAuth', data);
  }

  createUser(data: Request) {
    return this.msAuthClient.send<string, Request>('createUser', data);
  }

  updateUser(data: Request) {
    return this.msAuthClient.send<string, Request>('createUser', data);
  }

  deleteUser(data: string) {
    return this.msAuthClient.send<string, string>('deleteUser', data);
  }

  listUsers() {
    return this.msUserClient.send<string, boolean>('listUsers', true);
  }
}
