import { Controller, Get, Post, Body, Req, Put, Delete, Param } from '@nestjs/common';
import { AppService } from './app.service';
import { IReqLogin } from './interfaces/request.interface'
import { Request } from 'express';


@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}
  

  @Post('login')
  doLogin(@Body() data: IReqLogin) {
    return this.appService.doAuth(data);
  }

  @Post('user')
  createUser(@Req() data: Request) {
    return this.appService.createUser(data);
  }

  @Put('user')
  updateUser(@Req() data: Request) {
    return this.appService.updateUser(data);
  }
  @Delete('user')
  deleteUser(@Param() id: string) {
    return this.appService.deleteUser(id);
  }

  @Get('users')
  listUsers() {
    return this.appService.listUsers();
  }

}
